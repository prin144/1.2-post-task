<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div style="width: 900px;" class="form-group">
        
        <form method="POST" action="/posts">

            @csrf

            <div>
                <label for="title">Title: </label>
                <input class="form-control form-control-lg" id="title" name="title">
            </div>

            <div class="mb-4">
                <label for="content">Content: </label>
                <textarea style="resize: none; height: 100px;" class="form-control form-control-md" id="content" name="content"></textarea>
            </div>
            
            <button class="read-more-btn">Create</button>

            <a href="/posts" class="cncl-btn">Cancel</a>
        </form>
    </div>
</body>
</html>