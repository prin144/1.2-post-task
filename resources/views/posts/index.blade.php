<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div style="width: 900px;" class="container">
        <h1 class="text-left">Name of Blog</h1>

        <a class="read-more-btn" href="/posts/create" >Add Post</a>





        <div class="row">
        @foreach($posts as $post)

            <div class="col-md-4 d-flex">
                <div class="single-blog">
                    <p class="blog-meta">Post #{{ $post->id}} <span> {{ $post->updated_at}}</span></p>
                    <img src="https://us.123rf.com/450wm/pavelstasevich/pavelstasevich1811/pavelstasevich181101028/112815904-no-image-available-icon-flat-vector-illustration.jpg?ver=6">
                    <h2><a href="#" >{{ $post->title}}</a></h2>
                    <p class="blog-text">{{ $post->content}}</p>
                    <p><a href="/posts/{{ $post->id}}/edit" class="read-more-btn">Edit</a></p>
                </div>
            </div>
        
        @endforeach
        </div>

    </div>
</body>
</html>